package aeroport.vol;

import aeroport.exceptions.ExceptionServiceCreateVol;
import aeroport.exceptions.ExceptionServiceModifVol;
import aeroport.personne.NonNavigant;
import aeroport.personne.Service;
import aeroport.repository.VolRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestVol {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final EntityManager entityManager = Persistence.createEntityManagerFactory("Aeroport").createEntityManager();

    private NonNavigant nonNavigantVol;
    private NonNavigant nonNavigantVol2;

    @Before
    public void setUp() {
        Service serviceVol = new Service("Gestionnaire");
        nonNavigantVol = new NonNavigant("LAMBERT", "Thibaud", "34 Rue Guynemer", 637532876, serviceVol);

        Service serviceVol2 = new Service("Technique");
        nonNavigantVol2 = new NonNavigant("Jean", "Michel", "26 Rue du Brésil", 707070707, serviceVol2);
    }

    @Test
    public void testCreateVol() throws ExceptionServiceCreateVol {
        //Creation du vol
        Vol expected = nonNavigantVol.createVol(2);
        VolRepository volRepository = new VolRepository(entityManager);
        Vol actual = volRepository.save(expected);

        //Verifier que les vols sont egaux
        assertEquals(expected, actual);

        // Verifier qu'un Personnel autre que gestionnaire ne peut pas créer un vol

        //Definir qu'une exception doit etre levee pour l'execution suivante
        thrown.expect(ExceptionServiceCreateVol.class);

        //Cette création doit lever une erreur
        nonNavigantVol2.createVol(2);
    }

    @Test
    public void testModifVol() throws ExceptionServiceCreateVol, ExceptionServiceModifVol {
        //Creer vol
        Vol v = nonNavigantVol.createVol(12);
        VolRepository volRepository = new VolRepository(entityManager);
        //Enregistrer vol
        volRepository.save(v);
        //Modifier vol
        Vol newV = nonNavigantVol.modifVol(20, v);

        //Verifier resultat
        assertTrue(v.getFrequence() != newV.getFrequence());
        assertEquals(20, newV.getFrequence());

        //DEfinir qu'une exception doit etre levee pour l'execution suivante
        thrown.expect(ExceptionServiceModifVol.class);

        //Cette création doit lever une erreur
        nonNavigantVol2.modifVol(32, v);
    }
}
