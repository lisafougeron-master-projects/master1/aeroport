package aeroport.vol;

import aeroport.MainAeroport;
import aeroport.exceptions.ExceptionServiceCreateDepart;
import aeroport.personne.NonNavigant;
import aeroport.personne.Passager;
import aeroport.personne.Personnel;
import aeroport.personne.Service;
import aeroport.repository.DepartRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class TestDepart {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private NonNavigant nonNavigant;
    private NonNavigant nonNavigant2;

    private static final EntityManager entityManager = Persistence.createEntityManagerFactory("Aeroport").createEntityManager();

    @Before
    public void setUp() {
        Service service = new Service("Gestionnaire");
        nonNavigant = new NonNavigant("LAMBERT", "Thibaud", "34 Rue Guynemer", 637532876, service);

        Service service2 = new Service("Technique");
        nonNavigant2 = new NonNavigant("Jean", "Michel", "26 Rue du Brésil", 707070707, service2);
    }

    @Test
    public void testCreateDepart() throws ExceptionServiceCreateDepart {
        //Creation du depart
        Date date = MainAeroport.createDate("04/12/2018");
        Collection<Personnel> personnels = new ArrayList<>();
        Collection<Passager> passagers = new ArrayList<>();

        Depart expected = nonNavigant.createDepart(date, personnels, passagers);
        DepartRepository departRepository = new DepartRepository(entityManager);
        Depart actual = departRepository.save(expected);

        //Verifier que les Troncon sont egaux
        assertEquals(expected, actual);

        //DEfinir qu'une exception doit etre levee pour l'execution suivante
        thrown.expect(ExceptionServiceCreateDepart.class);

        //Cette création doit lever une erreur
        nonNavigant2.createDepart(date, personnels, passagers);
    }

}
