package aeroport.avion;

import aeroport.exceptions.ExceptionServiceCreateAvion;
import aeroport.exceptions.ExceptionServiceModifAvion;
import aeroport.personne.NonNavigant;
import aeroport.personne.Service;
import aeroport.repository.AvionRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestAvion {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final EntityManager entityManager = Persistence.createEntityManagerFactory("Aeroport").createEntityManager();

    private NonNavigant nonNavigantAvion;
    private NonNavigant nonNavigantAvion2;

    @Before
    public void setUp() {
        Service serviceAvion = new Service("Gestionnaire");
        nonNavigantAvion = new NonNavigant("LAMBERT", "Thibaud", "34 Rue Guynemer", 637532876, serviceAvion);

        Service serviceAvion2 = new Service("Technique");
        nonNavigantAvion2 = new NonNavigant("Jean", "Michel", "26 Rue du Brésil", 707070707, serviceAvion2);
    }

    @Test
    public void testCreateAvion() throws ExceptionServiceCreateAvion {
        //Creation d'un Avion
        Avion expected = nonNavigantAvion2.createAvion(200);
        AvionRepository avionRepository = new AvionRepository(entityManager);
        Avion actual = avionRepository.save(expected);

        //Verifier que les avion sont egaux
        assertEquals(expected, actual);

        // Verifier qu'un Personnel autre que technique ne peut pas créer un avion

        //Definir qu'une exception doit etre levee pour l'execution suivante
        thrown.expect(ExceptionServiceCreateAvion.class);

        //Cette création doit lever une erreur
        nonNavigantAvion.createAvion(150);
    }

    @Test
    public void testModifAvion() throws ExceptionServiceCreateAvion, ExceptionServiceModifAvion {
        //Creer avion
        Avion a = nonNavigantAvion2.createAvion(200);
        AvionRepository avionRepository = new AvionRepository(entityManager);
        //Enregistrer avion
        avionRepository.save(a);
        //Modifier avion
        Avion newA = nonNavigantAvion2.modifAvion(300, a);

        //Verifier resultat
        assertTrue(a.getCapacite() != newA.getCapacite());
        assertEquals(300, newA.getCapacite());

        //Definir qu'une exception doit etre levee pour l'execution suivante
        thrown.expect(ExceptionServiceModifAvion.class);

        //Cette création doit lever une erreur
        nonNavigantAvion.modifAvion(32, a);
    }
}
