package aeroport.avion;

import aeroport.exceptions.ExceptionServiceCreateClasseAvion;
import aeroport.personne.NonNavigant;
import aeroport.personne.Service;
import aeroport.repository.ClasseAvionRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;

public class TestClasseAvion {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final EntityManager entityManager = Persistence.createEntityManagerFactory("Aeroport").createEntityManager();

    private NonNavigant nonNavigantClasseAvion;
    private NonNavigant nonNavigantClasseAvion2;

    @Before
    public void setUp() {
        Service serviceClasseAvion = new Service("Gestionnaire");
        nonNavigantClasseAvion = new NonNavigant("LAMBERT", "Thibaud", "34 Rue Guynemer", 637532876, serviceClasseAvion);

        Service serviceClasseAvion2 = new Service("Technique");
        nonNavigantClasseAvion2 = new NonNavigant("Jean", "Michel", "26 Rue du Brésil", 707070707, serviceClasseAvion2);
    }


    @Test
    public void testCreateClasseAvion() throws ExceptionServiceCreateClasseAvion {
        //Creation d'une classeAvion
        ClasseAvion expected = nonNavigantClasseAvion2.createClasseAvion("Classe 1");
        ClasseAvionRepository classeAvionRepository = new ClasseAvionRepository(entityManager);
        ClasseAvion actual = classeAvionRepository.save(expected);

        //Verifier que les classeAvion sont egaux
        assertEquals(expected, actual);

        // Verifier qu'un Personnel autre que technique ne peut pas créer une classAvion

        //Definir qu'une exception doit etre levee pour l'execution suivante
        thrown.expect(ExceptionServiceCreateClasseAvion.class);

        //Cette création doit lever une erreur
        nonNavigantClasseAvion.createClasseAvion("Classe 2");
    }
}
