package aeroport.personne;

import aeroport.MainAeroport;
import aeroport.exceptions.ExceptionServiceCreateDepart;
import aeroport.exceptions.ExceptionServiceCreateTroncon;
import aeroport.exceptions.ExceptionServiceCreateVol;
import aeroport.repository.DepartRepository;
import aeroport.repository.TronconRepository;
import aeroport.repository.VolRepository;
import aeroport.vol.Depart;
import aeroport.vol.Troncon;
import aeroport.vol.Vol;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class NonNavigantTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final EntityManager entityManager = Persistence.createEntityManagerFactory("Aeroport").createEntityManager();

    private NonNavigant nN1;
    private NonNavigant nN2;

    @Before
    public void setUp() {
        Service s1 = new Service("Gestionnaire");
        nN1 = new NonNavigant("LAMBERT", "Thibaud", "34 Rue Guynemer", 637532876, s1);

        Service s2 = new Service("Technique");
        nN2 = new NonNavigant("Jean", "Michel", "26 Rue du Brésil", 707070707, s2);
    }


    @Test
    public void testAssocierDepartEtVol() throws ExceptionServiceCreateDepart, ExceptionServiceCreateVol {
        //Creation du depart
        Date date = MainAeroport.createDate("04/12/2018");
        Collection<Personnel> personnels = new ArrayList<>();
        Collection<Passager> passagers = new ArrayList<>();

        Depart dep = nN1.createDepart(date, personnels, passagers);
        DepartRepository departRepository = new DepartRepository(entityManager);
        dep = departRepository.save(dep);

        //Creation du vol
        Vol v = nN1.createVol(2);
        VolRepository volRepository = new VolRepository(entityManager);
        v = volRepository.save(v);

        departRepository.associerDepartEtVol(dep.getNumero(), v.getNumero());
        Depart expected = departRepository.findById(dep.getNumero());

        assertEquals(expected.getVol(), v);
    }

    @Test
    public void testCreateTroncon() throws ExceptionServiceCreateTroncon {
        //Creation du vol
        Troncon expected = nN1.createTroncon("La Rochelle", "Londres", 800);
        TronconRepository tronconRepository = new TronconRepository(entityManager);
        Troncon actual = tronconRepository.save(expected);

        //Verifier que les Troncon sont egaux
        assertEquals(expected, actual);

        //DEfinir qu'une exception doit etre levee pour l'execution suivante
        thrown.expect(ExceptionServiceCreateTroncon.class);

        //Cette création doit lever une erreur
        nN2.createTroncon("La Rochelle", "Bruxelles", 900);
    }
}