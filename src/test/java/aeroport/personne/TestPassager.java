package aeroport.personne;

import aeroport.repository.PassagerRepository;
import aeroport.vol.Depart;
import org.apache.log4j.Logger;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class TestPassager {

    private static final EntityManager entityManager = Persistence.createEntityManagerFactory("Aeroport").createEntityManager();
    private static final Logger LOGGER = Logger.getLogger(PassagerRepository.class);

    @Test
    public void testInsertPassagerNotInDB() {
        Passager passager = new Passager("Layne", "Alice", "2 Rue des Lilas", 123653410, 5555413);
        PassagerRepository passagerRepository = new PassagerRepository(entityManager);
        Passager pass = passagerRepository.save(passager);
        assertEquals(pass, passager);
    }

    @Test
    public void testInsertPassagerInDB() {
        Passager passager = new Passager("Cornell", "Chris", "12 Rue des Lilas", 123653410, 5555414);
        PassagerRepository passagerRepository = new PassagerRepository(entityManager);
        passagerRepository.save(passager);
        Passager pass2 = passagerRepository.save(passager);
        assertEquals(pass2, passager);
    }

    @Test
    public void testSearchPassagerInDB() {
        PassagerRepository passagerRepository = new PassagerRepository(entityManager);
        Passager passSearch = passagerRepository.search(5555414);
        LOGGER.info(passSearch);
        assertEquals("Passager{numPasseport=5555414, nom=\'Cornell\', prenom=\'Chris\'}", passSearch.toString());
    }

    @Test
    public void testSearchPassagerNotInDB() {
        PassagerRepository passagerRepository = new PassagerRepository(entityManager);
        Passager passSearch = passagerRepository.search(00000000);
        assertEquals(null, passSearch);
    }

    @Test
    public void testAssocPassager() {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        Passager passager = new Passager("Fougeron2", "Lisa", "2 Rue des Lilas", 123653410, 5555412);
        Date date = null;
        try {
            date = dateformat.parse("04/12/2018");
        } catch (ParseException e) {
            LOGGER.error(e.getMessage());
        }
        Depart depart = new Depart(date);
        PassagerRepository passagerRepository = new PassagerRepository(entityManager);
        boolean inserted = passagerRepository.associerPassager(passager, depart);
        assertEquals(true, inserted);
    }
}
