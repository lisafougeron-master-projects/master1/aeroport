package aeroport.personne;

import aeroport.repository.PersonnelRepository;
import aeroport.vol.Depart;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestPersonnel {
    private Personnel personnel;
    PersonnelRepository personnelRepository;

    private static final EntityManager entityManager = Persistence.createEntityManagerFactory("Aeroport").createEntityManager();
    private static final Logger LOGGER = Logger.getLogger(PersonnelRepository.class);

    @Before
    public void setUp() {
        Service service = new Service("Embarquement");
        personnel = new Personnel("Alexandre", "Baret", "10 Rue des Champottes", 656343410, service);
        personnelRepository = new PersonnelRepository();
        personnelRepository.setEntityManager(entityManager);
    }

    @Test
    public void testInsertPersonnel() {
        Personnel pers = personnelRepository.save(personnel);
        assertEquals(pers, personnel);
    }

    @Test
    public void testAssocPersonnel() {
        SimpleDateFormat dateFormatPers = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;

        try {
            date = dateFormatPers.parse("04/12/2018");
        } catch (ParseException e) {
            LOGGER.error(e.getMessage());
        }

        Depart depart = new Depart(date);
        boolean inserted = personnelRepository.associerPersonnel(personnel, depart);
        assertTrue(inserted);
    }

    @Test
    public void testConsulterPersonnel() {
        personnelRepository.listPersonnels();
        assertTrue(true);
    }
}
