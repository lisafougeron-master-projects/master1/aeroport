package aeroport.exceptions;

public class ExceptionServiceModifVol extends Exception {
    public ExceptionServiceModifVol() {
        super("Votre service n'est pas autorise pour modifier un vol");
    }
}
