package aeroport.exceptions;

public class ExceptionServiceCreateTroncon extends Exception{
    public ExceptionServiceCreateTroncon() {
        super("Votre service n'est pas autorise pour creer un tronçon");
    }
}
