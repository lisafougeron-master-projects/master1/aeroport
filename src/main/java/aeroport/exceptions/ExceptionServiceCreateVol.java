package aeroport.exceptions;

public class ExceptionServiceCreateVol extends Exception {
    public ExceptionServiceCreateVol() {
        super("Votre service n'est pas autorise pour creer un vol");
    }
}
