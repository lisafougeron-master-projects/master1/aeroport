package aeroport.exceptions;

public class ExceptionServiceCreateAvion extends Exception {
    public ExceptionServiceCreateAvion() {
        super("Votre service n'est pas autorise pour créer un avion");
    }
}
