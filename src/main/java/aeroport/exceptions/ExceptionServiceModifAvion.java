package aeroport.exceptions;

public class ExceptionServiceModifAvion extends Exception {
    public ExceptionServiceModifAvion() {
        super("Votre service n'est pas autorise pour modifier un avion");
    }
}
