package aeroport.exceptions;

public class ExceptionServiceCreateClasseAvion extends Exception {
    public ExceptionServiceCreateClasseAvion() {
        super("Votre service n'est pas autorise pour créer une ClasseAvion");
    }
}
