package aeroport.exceptions;

public class ExceptionServiceAssociateAvionToDepart extends Exception {
    public ExceptionServiceAssociateAvionToDepart() {
        super("Votre service n'est pas autorise pour associer un avion a un depart");
    }
}
