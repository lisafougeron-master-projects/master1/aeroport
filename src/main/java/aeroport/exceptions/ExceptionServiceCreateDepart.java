package aeroport.exceptions;

public class ExceptionServiceCreateDepart extends Exception {
    public ExceptionServiceCreateDepart() {
        super("Votre service n'est pas autorise pour creer un depart");
    }
}
