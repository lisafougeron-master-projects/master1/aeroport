package aeroport.state;

import aeroport.MainAeroport;
import aeroport.avion.Avion;
import aeroport.avion.ClasseAvion;
import aeroport.ihm.ChoixServiceTechnique;
import aeroport.ihm.DecorateurIhm;
import aeroport.repository.AvionRepository;
import aeroport.repository.ClasseAvionRepository;
import org.apache.log4j.Logger;

import java.util.List;

public class StateServiceTechnique implements State {
    private static final Logger LOGGER = Logger.getLogger(StateGestionnaire.class);
    private AvionRepository avionRepository = null;
    private ClasseAvionRepository classeAvionRepository = null;

    private AvionRepository getInstanceAvionRepo() {
        if (avionRepository == null) {
            avionRepository = new AvionRepository(MainAeroport.entityManager);
        }

        return avionRepository;
    }

    private ClasseAvionRepository getInstanceClasseAvionRepo() {
        if (classeAvionRepository == null) {
            classeAvionRepository = new ClasseAvionRepository(MainAeroport.entityManager);
        }

        return classeAvionRepository;
    }

    @Override
    public void doAction() {
        DecorateurIhm.afficheMenu();
        ChoixServiceTechnique.afficheMenu();
        choice();
    }

    @Override
    public void choice() {
        int choixInterface;

        do {
            choixInterface = MainAeroport.sc.nextInt();

            switch (choixInterface) {
                //Creer avion
                case 1:
                    creerAvion();
                    doAction();
                    break;
                //Afficher les avions
                case 2:
                    afficherAvions();
                    doAction();
                    break;
                //Modifier un avion
                case 3:
                    modifierAvion();
                    doAction();
                    break;
                //Creer une classe d'avion
                case 4:
                    creerClasseAvion();
                    doAction();
                    break;
                //Lister les classes d'avions
                case 5:
                    afficherClasseAvions();
                    doAction();
                    break;
                //Quitter
                case 6:
                    MainAeroport.context.setState(MainAeroport.STATEMAIN);
                    MainAeroport.context.doAction();
                    break;
                default:
                    break;
            }
        } while (choixInterface < 1 || choixInterface > 5);
    }

    private void creerAvion() {
        Avion a;
        LOGGER.info("Saisir la capacite de l'avion :");
        int capacite = MainAeroport.sc.nextInt();
        a = new Avion(capacite);
        if (getInstanceAvionRepo().save(a) != null) {
            LOGGER.info("Avion créé");
        } else {
            LOGGER.info("Avion non créé");
        }
    }

    private void afficherAvions() {
        List<Avion> avions;
        avions = getInstanceAvionRepo().findAll();
        if (avions != null) {
            for (Avion avion : avions) {
                LOGGER.info(avion.toString());
            }
        } else {
            LOGGER.info("Aucun avion créé");
        }
    }

    private void modifierAvion() {
        List<Avion> avions;
        Avion a;
        avions = getInstanceAvionRepo().findAll();
        for (Avion avion : avions) {
            LOGGER.info(avion.toString());
        }
        LOGGER.info("Choisir avion a modifier :");
        int avion = MainAeroport.sc.nextInt();
        a = getInstanceAvionRepo().modifAvion(avion);

        if (a != null) {
            LOGGER.info("Avion modifié");
        } else {
            LOGGER.info("Avion non modifié car il est introuvable");
        }
    }

    private void creerClasseAvion() {
        ClasseAvion ca;
        LOGGER.info("Saisir le libelle de la class d'avion :");
        MainAeroport.sc.nextLine();
        String libelle = MainAeroport.sc.nextLine();
        ca = new ClasseAvion(libelle);
        if (getInstanceClasseAvionRepo().save(ca) != null) {
            LOGGER.info("ClasseAvion créé");
        } else {
            LOGGER.info("ClasseAvion non créé");
        }
    }

    private void afficherClasseAvions() {
        List<ClasseAvion> classeAvions;
        classeAvions = getInstanceClasseAvionRepo().findAll();
        if (classeAvions != null) {
            for (ClasseAvion classeAvion : classeAvions) {
                LOGGER.info(classeAvion.toString());
            }
        } else {
            LOGGER.info("Aucune Classe Avion créé");
        }
    }

}
