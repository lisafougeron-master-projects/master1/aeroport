package aeroport.state;

import aeroport.MainAeroport;
import aeroport.ihm.ChoixGestionnaire;
import aeroport.ihm.DecorateurIhm;
import aeroport.personne.Personnel;
import aeroport.personne.Service;
import aeroport.repository.DepartRepository;
import aeroport.repository.PersonnelRepository;
import aeroport.repository.TronconRepository;
import aeroport.repository.VolRepository;
import aeroport.vol.Depart;
import aeroport.vol.Troncon;
import aeroport.vol.Vol;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class StateGestionnaire implements State {
    private static final Logger LOGGER = Logger.getLogger(StateGestionnaire.class);
    private VolRepository volRepository = null;
    private DepartRepository departRepository = null;
    private TronconRepository tronconRepository = null;
    private PersonnelRepository personnelRepository = null;

    private VolRepository getInstanceVolRepo() {
        if (volRepository == null) {
            volRepository = new VolRepository(MainAeroport.entityManager);
        }

        return volRepository;
    }

    private DepartRepository getInstanceDepartRepo() {
        if (departRepository == null) {
            departRepository = new DepartRepository(MainAeroport.entityManager);
        }

        return departRepository;
    }

    private TronconRepository getInstanceTronconRepo() {
        if (tronconRepository == null) {
            tronconRepository = new TronconRepository(MainAeroport.entityManager);
        }

        return tronconRepository;
    }

    private PersonnelRepository getInstancePersonnelRepo() {
        if (personnelRepository == null) {
            personnelRepository = new PersonnelRepository(MainAeroport.entityManager);
        }

        return personnelRepository;
    }

    @Override
    public void doAction() {
        DecorateurIhm.afficheMenu();
        ChoixGestionnaire.afficheMenu();
        choice();
    }

    @Override
    public void choice() {
        int choixInterface;

        do {
            choixInterface = MainAeroport.sc.nextInt();

            switch (choixInterface) {
                //Creer vol
                case 1:
                    creerVol();
                    doAction();
                    break;
                //Afficher les vols
                case 2:
                    afficherVols();
                    doAction();
                    break;
                //Modifier un vol
                case 3:
                    modifierVol();
                    doAction();
                    break;
                //Creer un depart
                case 4:
                    creerDepart();
                    doAction();
                    break;
                //Lister les departs
                case 5:
                    listerDeparts();
                    doAction();
                    break;
                //Creer un troncon
                case 6:
                    creerTroncon();
                    doAction();
                    break;
                //Lister les troncons
                case 7:
                    listerTroncon();
                    doAction();
                    break;
                //Associer vol depart
                case 8:
                    listerDeparts();
                    afficherVols();
                    associerVolDepart();
                    doAction();
                    break;
                //Quitter
                case 9:
                    MainAeroport.context.setState(MainAeroport.STATEMAIN);
                    MainAeroport.context.doAction();
                    break;
                default:
                    break;
            }
        } while (choixInterface < 1 || choixInterface > 9);
    }

    private void listerTroncon() {
        List<Troncon> troncons = getInstanceTronconRepo().findAll();
        if (troncons != null) {
            LOGGER.info("***** TRONCONS *****");
            for (Troncon tr : troncons) {
                LOGGER.info(tr.toString());
            }
        } else {
            LOGGER.info("Aucun tronçon créé");
        }
    }

    private void creerTroncon() {
        Troncon t = saisieTroncon();
        if (getInstanceTronconRepo().save(t) != null) {
            LOGGER.info("Troncon créé");
        } else {
            LOGGER.info("Troncon non créé");
        }
    }

    private void listerDeparts() {
        List<Depart> departs = getInstanceDepartRepo().findAll();
        if (departs != null) {
            LOGGER.info("***** DEPARTS *****");
            for (Depart depart : departs) {
                LOGGER.info(depart.toString());
            }
        } else {
            LOGGER.info("Aucun tronçon créé");
        }
    }

    private void creerDepart() {
        LOGGER.info("Saisir date (DD/MM/YYYY) :");
        String date;

        do {
            date = MainAeroport.sc.nextLine();
        } while (date == null || date.isEmpty());
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

        Date d = null;
        try {
            d = dateformat.parse(date);
        } catch (ParseException e) {
            LOGGER.error(e.getMessage());
        }

        Depart dep = new Depart(d);

        // Ajout des personnels au départ

        // Récupération d'un personnel pour l'ajouter à la liste des personnels du départ
        Personnel personnel = null;
        List<Personnel> personnelList = getInstancePersonnelRepo().findAll();

        if (!personnelList.isEmpty()) {
            personnel = personnelList.get(0);
        }

        // Si aucun personnel, on en ajoute un
        if (personnel == null) {
            Service service = new Service("Embarquement");
            personnel = new Personnel("Alexandre", "Baret", "10 Rue des Champottes", 06457624, service);
        }

        getInstancePersonnelRepo().associerPersonnel(personnel, dep);

        if (getInstanceDepartRepo().save(dep) != null) {
            LOGGER.info("Depart créé");
        } else {
            LOGGER.info("Depart non créé");
        }
    }

    private void modifierVol() {
        List<Vol> vols;
        Vol v;
        vols = getInstanceVolRepo().findAll();
        for (Vol vol : vols) {
            LOGGER.info(vol.toString());
        }
        LOGGER.info("Choisir vol a modifier :");
        int avion = MainAeroport.sc.nextInt();
        v = getInstanceVolRepo().modifVol(avion);

        if (v != null) {
            LOGGER.info("Vol modifié");
        } else {
            LOGGER.info("Vol non modifié car il est introuvable");
        }
    }

    private void afficherVols() {
        List<Vol> vols;
        vols = getInstanceVolRepo().findAll();
        if (vols != null) {
            LOGGER.info("***** VOLS *****");
            for (Vol vol : vols) {
                LOGGER.info(vol.toString());
            }
        } else {
            LOGGER.info("Aucun vol créé");
        }
    }

    private void creerVol() {
        Vol v;
        LOGGER.info("Saisir la fréquence du vol :");
        int frequence = MainAeroport.sc.nextInt();
        v = new Vol(frequence);
        if (getInstanceVolRepo().save(v) != null) {
            LOGGER.info("Vol créé");
        } else {
            LOGGER.info("Vol non créé");
        }
    }

    private Troncon saisieTroncon() {
        LOGGER.info("Saisir la ville de départ du tronçon :");
        String vd;

        do {
            vd = MainAeroport.sc.nextLine();
        } while (vd == null || vd.isEmpty());


        LOGGER.info("Saisir la ville d'arrivee du tronçon :");
        String va;

        do {
            va = MainAeroport.sc.nextLine();
        } while (va == null || va.isEmpty());

        LOGGER.info("Saisir la distance du tronçon :");
        long d = 0;

        do {
            d = MainAeroport.sc.nextLong();
        } while (d == 0);

        return new Troncon(vd, va, d);
    }

    private void associerVolDepart() {
        int idVol = -1;
        int idDepart = -1;

        LOGGER.info("Choisir depart :");
        do {
            idDepart = MainAeroport.sc.nextInt();
        } while (idDepart == -1);

        LOGGER.info("Choisir vol :");
        do {
            idVol = MainAeroport.sc.nextInt();
        } while (idVol == -1);

        Depart d = getInstanceDepartRepo().associerDepartEtVol(idDepart, idVol);

        if (d != null) {
            LOGGER.info("Vol associé");
        } else {
            LOGGER.info("Vol non associé");
        }
    }
}
