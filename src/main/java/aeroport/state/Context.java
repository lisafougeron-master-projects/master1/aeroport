package aeroport.state;

public class Context implements State {
    private State state;

    public Context() {
        this.state = null;
    }

    public Context(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public void doAction() {
        this.state.doAction();
    }

    @Override
    public void choice() {
        // nop
    }
}
