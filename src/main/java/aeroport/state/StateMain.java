package aeroport.state;

import aeroport.MainAeroport;

public class StateMain implements State {

    /**
     * Retourne à l'état initial du Menu
     */
    @Override
    public void doAction() {
        MainAeroport.ihm.afficherTexte();

        int choixInterface;

        do {
            choixInterface = MainAeroport.sc.nextInt();

            switch (choixInterface) {
                case 1:
                    MainAeroport.context.setState(MainAeroport.PERSONNEL);
                    MainAeroport.context.doAction();
                    break;
                case 2:
                    MainAeroport.context.setState(MainAeroport.PASSAGER);
                    MainAeroport.context.doAction();
                    break;
                case 3:
                    MainAeroport.context.setState(MainAeroport.GESTIONNAIRE);
                    MainAeroport.context.doAction();
                    break;
                case 4:
                    MainAeroport.context.setState(MainAeroport.TECHNIQUE);
                    MainAeroport.context.doAction();
                    return;
                case 5:
                    return;
                default:
                    break;
            }
        } while (choixInterface < 1 || choixInterface > 4);
    }

    @Override
    public void choice() {
        // nop
    }
}
