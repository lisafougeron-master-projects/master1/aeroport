package aeroport.state;

import aeroport.MainAeroport;
import aeroport.ihm.ChoixGestionPersonnel;
import aeroport.ihm.DecorateurIhm;
import aeroport.repository.PersonnelRepository;
import aeroport.repository.ServiceRepository;
import org.apache.log4j.Logger;

public class StateGestionPersonnel implements State {
    private static final Logger LOGGER = Logger.getLogger(StateGestionPersonnel.class);
    private ServiceRepository serviceRepository;
    private PersonnelRepository personnelRepository;

    private PersonnelRepository getInstancePersonnelRepo() {
        if (personnelRepository == null) {
            personnelRepository = new PersonnelRepository(MainAeroport.entityManager);
        }

        return personnelRepository;
    }

    private ServiceRepository getInstanceServiceRepo() {
        if (serviceRepository == null) {
            serviceRepository = new ServiceRepository(MainAeroport.entityManager);
        }

        return serviceRepository;
    }

    @Override
    public void doAction() {
        DecorateurIhm.afficheMenu();
        ChoixGestionPersonnel.afficheMenu();
        choice();
    }

    @Override
    public void choice() {
        int choix;

        do {
            choix = MainAeroport.sc.nextInt();

            switch (choix) {
                case 1:
                    boolean personnelInserted = getInstancePersonnelRepo().addPersonnel();

                    if (personnelInserted) {
                        LOGGER.info("Personnel ajouté ");
                    } else {
                        LOGGER.info("L'ajout du personnel à échoué");
                    }
                    doAction();
                    break;
                case 2:
                    getInstancePersonnelRepo().listPersonnels();
                    doAction();
                    break;
                case 3:
                    boolean success = getInstanceServiceRepo().addService();

                    if (success) {
                        LOGGER.info("Service ajouté !");
                    } else {
                        LOGGER.info("Erreur durant l'insertion...");
                    }

                    doAction();
                    break;
                case 4:
                    getInstanceServiceRepo().listServices();
                    doAction();
                    break;
                case 5:
                    MainAeroport.context.setState(MainAeroport.STATEMAIN);
                    MainAeroport.context.doAction();
                    break;
                default:
                    break;
            }
        } while (choix < 2 || choix > 4);
    }
}
