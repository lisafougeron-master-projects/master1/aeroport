package aeroport.state;

import aeroport.MainAeroport;
import aeroport.ihm.ChoixGestionEmbarquement;
import aeroport.ihm.DecorateurIhm;
import aeroport.personne.Passager;
import aeroport.repository.DepartRepository;
import aeroport.repository.PassagerRepository;
import aeroport.vol.Depart;
import org.apache.log4j.Logger;

import java.util.List;

public class StateGestionEmbarquement implements State {
    private static final Logger LOGGER = Logger.getLogger(StateGestionEmbarquement.class);
    private PassagerRepository passagerRepository;
    private DepartRepository departRepository;
    private Passager passager;

    /**
     * Fonction qui permet de recuperer une instance du controleur de la classe Passager
     *
     * @return DepartRepository le controleur de la classe Passager
     */
    private PassagerRepository getInstancePassagerRepo() {
        if (passagerRepository == null) {
            passagerRepository = new PassagerRepository(MainAeroport.entityManager);
        }
        return passagerRepository;
    }

    /**
     * Fonction qui permet de recuperer une instance du controleur de la classe Depart
     *
     * @return DepartRepository le controleur de la classe Depart
     */
    private DepartRepository getInstanceDepartRepo() {
        if (departRepository == null) {
            departRepository = new DepartRepository(MainAeroport.entityManager);
        }
        return departRepository;
    }

    /**
     * Fonction redefinie qui permet d'afficher le menu correspondant à l'interface
     */
    @Override
    public void doAction() {
        DecorateurIhm.afficheMenu();
        ChoixGestionEmbarquement.afficheMenu();
        choice();
    }

    /**
     * Fonction redefinie qui indique les fonctionnalites relatives au menu
     */
    @Override
    public void choice() {
        int choix;

        do {
            choix = MainAeroport.sc.nextInt();
            switch (choix) {
                case 1:
                    // on recupere le passager qu'il soit deja en base ou s'il vient d etre saisi
                    this.passager = getInstancePassagerRepo().addPassager();
                    if (passager != null) {
                        LOGGER.info("Passager a été récupéré avec succès");
                    } else {
                        LOGGER.info("Echec de recuperation du passager");
                    }
                    doAction();
                    break;
                case 2:
                    getInstancePassagerRepo().listPassagers();
                    doAction();
                    break;
                case 3:
                    // saisie de la date du depart
                    String date = saisieDate();
                    this.associerPassagerDepart(date);
                    doAction();
                    break;
                case 4:
                    MainAeroport.context.setState(MainAeroport.STATEMAIN);
                    MainAeroport.context.doAction();
                    break;
                default:
                    break;
            }
        } while (choix < 2 || choix > 4);
    }

    private void associerPassagerDepart(String date) {
        // la liste de tous les departs pour une date donnee
        List<Depart> liste = getInstanceDepartRepo().findByDate(date);
        if(liste!=null) {
            // affichage de la liste
            Depart depart = getInstanceDepartRepo().findDepartEmbarquement(liste);
            // association entre le passager selectionne et le depart selectionné
            boolean embarquementInserted = getInstancePassagerRepo().associerPassager(this.passager, depart);
            if (embarquementInserted) {
                LOGGER.info("Passager a été enregistré pour l'embarquement Numéro :" + depart.getNumero());
            } else {
                LOGGER.info("Echec de l'enregistrement du passager pour l'embarquement Numéro :" + depart.getNumero());
            }
            getInstancePassagerRepo().printCard(passager, depart);
        }else{
            LOGGER.info("Aucun depart trouvé pour cette date");
        }
    }
    /**
     * Fonction de saisie de la date qui verifie le format d'entree dd/mm/yyyy
     *
     * @return la date sous forme d'une String
     */
    private String saisieDate() {
        // Date
        LOGGER.info("Date au format dd/mm/yyyy:");
        String date;
        // on saisie la date tant qu'elle n'est pas correcte
        do {
            date = MainAeroport.sc.nextLine();
        }
        while (date == null || date.isEmpty() || !date.matches("(0[1-9]|[12]\\d|3[01])/(0[1-9]|1[0-2])/([12]\\d{3})"));
        return date;
    }
}
