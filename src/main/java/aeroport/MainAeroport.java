package aeroport;

import aeroport.ihm.ImplementationIhm;
import aeroport.state.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Logger;

public class MainAeroport {
    public static final Scanner sc = new Scanner(System.in);
    public static final Context context = new Context();
    public static final ImplementationIhm ihm = new ImplementationIhm();

    // Déclarations des Etats possibles
    public static final StateMain STATEMAIN = new StateMain();
    public static final StateGestionPersonnel PERSONNEL = new StateGestionPersonnel();
    public static final StateGestionEmbarquement PASSAGER = new StateGestionEmbarquement();
    public static final StateGestionnaire GESTIONNAIRE = new StateGestionnaire();
    public static final StateServiceTechnique TECHNIQUE = new StateServiceTechnique();
    public static final Logger LOGGER = Logger.getGlobal();

    private static final int QUITTER = 5;

    public static final EntityManager entityManager = Persistence.createEntityManagerFactory("Aeroport").createEntityManager();

    public static void main(String[] args) {
        int choixUtilisateur = 0;

        // Affichage interface utilisateur
        ihm.afficherTexte();

        while (choixUtilisateur < 1 || choixUtilisateur > 1) {
            // Saisie utilisateur
            choixUtilisateur = sc.nextInt();

            switch (choixUtilisateur) {
                case 1:
                    context.setState(PERSONNEL);
                    context.doAction();
                    break;
                case 2:
                    context.setState(PASSAGER);
                    context.doAction();
                    break;
                case 3:
                    context.setState(GESTIONNAIRE);
                    context.doAction();
                    break;
                case 4:
                    context.setState(TECHNIQUE);
                    context.doAction();
                    break;
                case QUITTER:
                    return;
                default:
                    break;
            }
        }
    }

    public static Date createDate(String s){
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;

        if(s == null || s.isEmpty() || !s.matches("(0[1-9]|[12]\\d|3[01])/(0[1-9]|1[0-2])/([12]\\d{3})")) {
            LOGGER.warning("ERREUR FORMAT DATE");
        }else{
            try {
                d = dateformat.parse(s);
            } catch (ParseException e) {
                LOGGER.warning(e.getMessage());
            }
        }

        return d;
    }
}
