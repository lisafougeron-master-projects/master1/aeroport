package aeroport.ihm;

import org.apache.log4j.Logger;

public abstract class ChoixServiceTechnique extends DecorateurIhm {
    private static final Logger LOGGER = Logger.getLogger(ChoixServiceTechnique.class);

    public static void afficheMenu() {
        LOGGER.info("1 - Créer un avion");
        LOGGER.info("2 - Lister les avions");
        LOGGER.info("3 - Modifier un avion");
        LOGGER.info("4 - Créer une classe d'avion");
        LOGGER.info("5 - Lister les classe d'avions");
        LOGGER.info("6 - Quitter");
    }
}
