package aeroport.ihm;

import org.apache.log4j.Logger;


public abstract class ChoixGestionEmbarquement extends DecorateurIhm{
    private static final Logger LOGGER = Logger.getLogger(ChoixGestionEmbarquement.class);

    public static void afficheMenu() {
        LOGGER.info("1 - Chercher un passager");
        LOGGER.info("2 - Lister les passagers");
        LOGGER.info("3 - Enregistrer un embarquement");
        LOGGER.info("4 - Quitter");
    }
}
