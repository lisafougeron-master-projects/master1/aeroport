package aeroport.ihm;

import org.apache.log4j.Logger;

public class ImplementationIhm implements AbstractionIhm {
    private static final Logger LOGGER = Logger.getLogger(ImplementationIhm.class);

    /**
     * Constructeur par défaut
     */
    public ImplementationIhm() {
    }

    @Override
    public void afficherTexte() {
        DecorateurIhm.afficheMenu();
        LOGGER.info("1 - Interface Gestion Personnel");
        LOGGER.info("2 - Interface Gestion Embarquement");
        LOGGER.info("3 - Interface Gestionnaire");
        LOGGER.info("4 - Interface Technicien");
        LOGGER.info("5 - Quitter");
    }
}
