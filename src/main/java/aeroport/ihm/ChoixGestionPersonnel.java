package aeroport.ihm;

import org.apache.log4j.Logger;

public abstract class ChoixGestionPersonnel extends DecorateurIhm {
    private static final Logger LOGGER = Logger.getLogger(ChoixGestionPersonnel.class);
    public static void afficheMenu() {
        LOGGER.info("1 - Ajouter un Personnel");
        LOGGER.info("2 - Lister Personnels");
        LOGGER.info("3 - Ajouter un Service");
        LOGGER.info("4 - Lister Services");
        LOGGER.info("5 - Quitter");
    }
}
