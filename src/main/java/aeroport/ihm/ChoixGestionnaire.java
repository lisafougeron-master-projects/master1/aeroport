package aeroport.ihm;

import org.apache.log4j.Logger;

public abstract class ChoixGestionnaire extends DecorateurIhm {
    private static final Logger LOGGER = Logger.getLogger(ChoixGestionnaire.class);

    public static void afficheMenu() {
        LOGGER.info("1 - Créer un vol");
        LOGGER.info("2 - Lister les vols");
        LOGGER.info("3 - Modifier un vol");
        LOGGER.info("4 - Créer un départ");
        LOGGER.info("5 - Lister les départs");
        LOGGER.info("6 - Créer un tronçon");
        LOGGER.info("7 - Lister les tronçons");
        LOGGER.info("8 - Associer un vol à un départ");
        LOGGER.info("9 - Quitter");
    }
}
