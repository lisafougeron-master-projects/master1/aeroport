package aeroport.avion;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(schema = "AEROPORT")
public class ClasseAvion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int numero;
    private String libelle;

    @OneToMany(mappedBy = "classeAvion")
    private Collection<Avion> avions = new ArrayList<>();

    /**
     * Constructeur par défaut
     */
    public ClasseAvion() {
    }

    /**
     * Contructeur
     * @param libelle String
     */
    public ClasseAvion(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Contructeur
     * @param numero int
     * @param libelle String
     * @param avions Collection<Avion>
     */
    public ClasseAvion(int numero, String libelle, Collection<Avion> avions) {
        this.numero = numero;
        this.libelle = libelle;
        this.avions = avions;
    }

    /**
     * Retourne le numéro de la classe d'avion
     * @return int
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Modifie le numéro de la classe d'avion
     * @param numero int
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Retourne le libelle de la classe d'avion
     * @return String
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Modifie le libelle de la classe d'avion
     * @param libelle String
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Retourne la collection d'avions
     * @return Collection<Avion>
     */
    public Collection<Avion> getAvions() {
        return avions;
    }

    /**
     * Modifie la collection d'avions
     * @param avions Collection<Avion>
     */
    public void setAvions(Collection<Avion> avions) {
        this.avions = avions;
    }
}
