package aeroport.avion;

import aeroport.vol.Depart;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(schema = "AEROPORT")
public class DepartAvion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int numero;
    private int quantiteCarburant;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Avion avion;

    @OneToMany(mappedBy = "departAvion")
    private Collection<Depart> departs = new ArrayList<>();

    /**
     * Contructeur par défaut
     */
    public DepartAvion() {
    }

    /**
     * Contructeur
     * @param quantiteCarburant int
     * @param avion Avion
     * @param departs Collection<Depart>
     */
    public DepartAvion(int quantiteCarburant, Avion avion, Collection<Depart> departs) {
        this.quantiteCarburant = quantiteCarburant;
        this.avion = avion;
        this.departs = departs;
    }

    /**
     * Retourne le numéro
     * @return int
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Modifie le numéro
     * @param numero int
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Retourne la quantité de carburant dans l'avion
     * @return int
     */
    public int getQuantiteCarburant() {
        return quantiteCarburant;
    }

    /**
     * Modifie la quantité de carburant dans l'avion
     * @param quantiteCarburant int
     */
    public void setQuantiteCarburant(int quantiteCarburant) {
        this.quantiteCarburant = quantiteCarburant;
    }

    /**
     * Retourne l'avion
     * @return Avion
     */
    public Avion getAvion() {
        return avion;
    }

    /**
     * Modifie l'avion
     * @param avion Avion
     */
    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    /**
     * Retourne les départs
     * @return Collection<Depart>
     */
    public Collection<Depart> getDeparts() {
        return departs;
    }

    /**
     * Modifie les départs
     * @param departs Collection<Depart>
     */
    public void setDeparts(Collection<Depart> departs) {
        this.departs = departs;
    }
}
