package aeroport.avion;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(schema = "AEROPORT")
public class Avion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int numero;
    private int capacite;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private ClasseAvion classeAvion;

    @OneToMany(mappedBy = "avion")
    private Collection<DepartAvion> departsAvion = new ArrayList<>();

    /**
     * Contructeur par défaut
     */
    public Avion() {
    }

    /**
     * Contructeur
     * @param capacite int
     */
    public Avion(int capacite) {
        this.capacite = capacite;
    }

    /**
     * Constructeur
     * @param capacite int
     * @param classeAvion ClasseAvion
     */
    public Avion(int capacite, ClasseAvion classeAvion, Collection<DepartAvion> departsAvion) {
        this.capacite = capacite;
        this.classeAvion = classeAvion;
        this.departsAvion = departsAvion;
    }

    /**
     * Retourne le numéro de l'avion
     * @return int
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Modifie le numéro de l'avion
     * @param numero int
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Retourne la capacité de l'avion
     * @return int
     */
    public int getCapacite() {
        return capacite;
    }

    /**
     * Modifie la capacité de l'avion
     * @param capacite int
     */
    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    /**
     * Retourne la classeAvion de l'avion
     * @return ClasseAvion
     */
    public ClasseAvion getClasseAvion() {
        return classeAvion;
    }

    /**
     * Modifie la classeAvion de l'avion
     * @param classeAvion ClasseAvion
     */
    public void setClasseAvion(ClasseAvion classeAvion) {
        this.classeAvion = classeAvion;
    }

    /**
     * Retourne les départsAvion
     * @return DepartAvion
     */
    public Collection<DepartAvion> getDepartsAvion() {
        return departsAvion;
    }

    /**
     * Modifie les départsAvion
     * @param departsAvion DepartAvion
     */
    public void setDepartsAvion(Collection<DepartAvion> departsAvion) {
        this.departsAvion = departsAvion;
    }
}
