package aeroport.repository;

import aeroport.vol.Troncon;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class TronconRepository {
    private EntityManager entityManager;

    public TronconRepository() {
    }

    public TronconRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Troncon save(Troncon t) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.persist(t);
        Troncon e = entityManager.find(Troncon.class, t.getNumero());
        entityTransaction.commit();

        return e;
    }

    public List<Troncon> findAll() {
        String queryStr = "SELECT t FROM Troncon t";
        Query query = entityManager.createQuery(queryStr);
        return query.getResultList();
    }

    public Troncon findOne(int id) {
        Troncon t = null;
        String queryStr = "SELECT t FROM Troncon t where t.numero=:num";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("num", id);
        List<Troncon> troncons = query.getResultList();

        if (troncons != null && !troncons.isEmpty()) {
            t = troncons.get(0);
        }

        return t;
    }
}
