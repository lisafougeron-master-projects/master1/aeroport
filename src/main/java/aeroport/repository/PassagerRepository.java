package aeroport.repository;

import aeroport.MainAeroport;
import aeroport.personne.Passager;
import aeroport.vol.Depart;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class PassagerRepository {
    private static final Logger LOGGER = Logger.getLogger(PassagerRepository.class);
    private EntityManager entityManager;

    /**
     * Constructeur par defaut
     */
    public PassagerRepository() {
    }

    /**
     * Constructeur du controleur de la classe Passager
     *
     * @param entityManager le controleur de Passager
     */
    public PassagerRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Methode permettant d'enregistrer un passager dans la base
     *
     * @param passager de type Passager, celui que l on veut enregistrer
     * @return le passager que l on a enregistre
     */
    public Passager save(Passager passager) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(passager);
        Passager pass = entityManager.find(Passager.class, passager.getNumero());
        entityTransaction.commit();
        return pass;
    }

    /**
     * Methode permettant de rechercher un passager en base
     *
     * @param numPasseport passager que l on recherche
     * @return le passager que l on recherche ou bien null s il n est pas en base
     */
    public Passager search(long numPasseport) {
        return findByPasseport(numPasseport);
    }

    /**
     * Fonction permettant de rechercher et de retourner un passager trouve en base
     *
     * @param numPasseport un long qui est le numero de passeport du passager
     * @return passager que l'on a trouve en base
     */
    public Passager findByPasseport(long numPasseport) {
        Passager passager = null;

        // Exécution de la requête et récupération résultat
        String queryStr = "SELECT p FROM Passager p WHERE p.numPasseport = :num";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("num", numPasseport);

        List<Passager> res = query.getResultList();

        if (res != null && !res.isEmpty()) {
            passager = res.get(0);
        }

        return passager;
    }

    /**
     * Fonction permettant de rechercher un passager et de le recuperer depuis la base
     * ou d'ajouter un passager (si la recherche n'a pas retournee de passager)
     *
     * @return passager que l'on veut embarquer
     */
    public Passager addPassager() {
        LOGGER.info("---- Interface d'ajout d'un Passager ----");
        //chercher passaport
        long numpassport = setNumPasseport();
        //RECHERCHE DU PASSAGER EN BASE
        Passager passager = findByPasseport(numpassport);
        if (passager != null) {
            LOGGER.info("Le passager correspondant à ce numéro de passeport a été trouvé :");
            return passager;
        }
        return save(setSaisiePassager());
    }

    /**
     * Fonction permettant la saisie par l'utilisateur d'un passager a enregistrer
     *
     * @return passager le Passager a inserer en base
     */
    public Passager setSaisiePassager() {
        LOGGER.info("Passager non enregistré, veuillez saisir ses données :");
        // NOM
        LOGGER.info("Nom Passager :");
        String nom;

        do {
            nom = MainAeroport.sc.nextLine();
        } while (nom == null || nom.isEmpty());

        // PRENOM
        LOGGER.info("Prénom Passager :");
        String prenom;

        do {
            prenom = MainAeroport.sc.nextLine();
        } while (prenom == null || prenom.isEmpty());

        // ADRESSE
        LOGGER.info("Adresse Passager :");
        String adresse;

        do {
            adresse = MainAeroport.sc.nextLine();
        } while (adresse == null || adresse.isEmpty());

        // NUM TEL
        LOGGER.info("Numéro Tel Passager :");
        long numTel = 0;

        do {
            numTel = MainAeroport.sc.nextLong();
        } while (numTel <= 0);

        // NUM PASSEPORT
        LOGGER.info("Numero de passeport :");
        long numpassport;

        do {
            numpassport = MainAeroport.sc.nextLong();
        } while (numpassport == 0);

        return new Passager(nom, prenom, adresse, numTel, numpassport);
    }

    /**
     * Fonction permettant la saisie par l'utilisateur d'un numero de passeport
     *
     * @return numpassport un long
     */
    private long setNumPasseport() {
        // NUM PASSEPORT
        LOGGER.info("Numero de passeport :");
        long numpassport;

        do {
            numpassport = MainAeroport.sc.nextLong();
        } while (numpassport == 0);
        return numpassport;
    }


    /**
     * Fonction d'affichage de la liste des passagers inscrits en base
     */
    public void listPassagers() {
        List<Passager> passagerList = findAll();
        for (Passager passager : passagerList) {
            LOGGER.info(passager.getNumero() + " - " + passager.getNom() + ", " + passager.getPrenom() + ", " + passager.getNumPasseport());
        }
    }

    /**
     * Retourne la liste des Passagers
     *
     * @return List    Liste des passagers existants
     */
    public List<Passager> findAll() {
        String queryStr = "SELECT p FROM Passager p";
        Query query = entityManager.createQuery(queryStr);
        return query.getResultList();
    }

    /**
     * Fonction qui permet d'associer un passager a un depart
     *
     * @param passager Passager a embarquer
     * @param depart   Depart sur lequel le passager veut embarquer
     * @return boolean indiquant si l'association s'est bien deroulée
     */
    public boolean associerPassager(Passager passager, Depart depart) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        depart.getPassagers().add(passager);
        passager.getDeparts().add(depart);
        entityManager.persist(passager);
        entityManager.persist(depart);
        entityTransaction.commit();
        return true;
    }

    public void printCard(Passager passager, Depart depart) {
        LOGGER.info(passager.printPassager() + " est prié d'embarquer pour le vol " + depart.getVol().getNumero());
    }
}
