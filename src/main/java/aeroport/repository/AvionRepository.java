package aeroport.repository;

import aeroport.MainAeroport;
import aeroport.avion.Avion;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class AvionRepository {
    private static final Logger LOGGER = Logger.getLogger(AvionRepository.class);
    private EntityManager entityManager;

    public AvionRepository() {
    }

    public AvionRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Avion save(Avion a) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.persist(a);
        Avion e = entityManager.find(Avion.class, a.getNumero());
        entityTransaction.commit();

        return e;
    }

    public List<Avion> findAll() {
        String queryStr = "SELECT a FROM Avion a";
        Query query = entityManager.createQuery(queryStr);
        return query.getResultList();
    }

    public Avion findOne(int id) {
        Avion a = null;
        String queryStr = "SELECT a FROM Avion a where a.numero=:num";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("num", id);
        List<Avion> as = query.getResultList();

        if (as != null && !as.isEmpty()) {
            a = as.get(0);
        }

        return a;
    }

    public Avion modifAvion(int id) {
        EntityTransaction entityTransaction = entityManager.getTransaction();

        entityTransaction.begin();

        //Recuperer l'avion a en DB
        Avion a = entityManager.find(Avion.class, id);

        if (a == null) {
            return null;
        } else {
            LOGGER.info("Saisir la nouvelle capacite du vol :");
            int capacite = MainAeroport.sc.nextInt();
            a.setCapacite(capacite);
            entityManager.flush();

            a = entityManager.find(Avion.class, a.getNumero());
        }
        entityTransaction.commit();

        return a;
    }

    public Avion modifAvion(int capacite, Avion v) {
        EntityTransaction entityTransaction = entityManager.getTransaction();

        entityTransaction.begin();

        //Recuperer l'avion v en DB
        Avion e = entityManager.find(Avion.class, v.getNumero());

        if (e == null) {
            return null;
        } else {
            e.setCapacite(capacite);
            entityManager.flush();

            e = entityManager.find(Avion.class, e.getNumero());
        }
        entityTransaction.commit();

        return e;
    }
}
