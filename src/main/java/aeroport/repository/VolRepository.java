package aeroport.repository;

import aeroport.MainAeroport;
import aeroport.vol.Vol;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class VolRepository {
    private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(VolRepository.class);

    public VolRepository() {
    }

    public VolRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Vol save(Vol v) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.persist(v);
        Vol e = entityManager.find(Vol.class, v.getNumero());
        entityTransaction.commit();

        return e;
    }

    public List<Vol> findAll() {
        String queryStr = "SELECT v FROM Vol v";
        Query query = entityManager.createQuery(queryStr);
        return query.getResultList();
    }

    public Vol findOne(int id) {
        Vol v = null;
        String queryStr = "SELECT v FROM Vol v where v.numero=:num";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("num", id);
        List<Vol> vols = query.getResultList();

        if (vols != null && !vols.isEmpty()) {
            v = vols.get(0);
        }

        return v;
    }

    public Vol modifVol(int id) {
        EntityTransaction entityTransaction = entityManager.getTransaction();

        entityTransaction.begin();

        //Recuperer le vol v en DB
        Vol v = entityManager.find(Vol.class, id);

        if (v == null) {
            return null;
        } else {
            LOGGER.info("Saisir la nouvelle frequence du vol :");
            int frequence = MainAeroport.sc.nextInt();
            v.setFrequence(frequence);
            entityManager.flush();

            v = entityManager.find(Vol.class, v.getNumero());
        }
        entityTransaction.commit();

        return v;
    }

    public Vol modifVol(int frequence, Vol v) {
        EntityTransaction entityTransaction = entityManager.getTransaction();

        entityTransaction.begin();

        //Recuperer le vol v en DB
        Vol e = entityManager.find(Vol.class, v.getNumero());

        if (e == null) {
            return null;
        } else {
            e.setFrequence(frequence);
            entityManager.flush();

            e = entityManager.find(Vol.class, e.getNumero());
        }
        entityTransaction.commit();

        return e;
    }
}