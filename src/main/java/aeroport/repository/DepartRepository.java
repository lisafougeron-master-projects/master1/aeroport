package aeroport.repository;

import aeroport.MainAeroport;
import aeroport.vol.Depart;
import aeroport.vol.Vol;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class DepartRepository {
    private static final Logger LOGGER = Logger.getLogger(DepartRepository.class);
    private EntityManager entityManager;

    /**
     * Constructeur par defaut
     */
    public DepartRepository() {
    }

    public DepartRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Depart save(Depart d) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.persist(d);
        Depart e = entityManager.find(Depart.class, d.getNumero());
        entityTransaction.commit();

        return e;
    }

    public Depart associerDepartEtVol(int idDepart, int idVol) {
        Depart d =null;
        boolean volExist = false;
        Vol v = null;

        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        VolRepository vr = new VolRepository(entityManager);
        volExist = vr.findOne(idVol) != null;

        if(volExist) {
            v = vr.findOne(idVol);

            //Recuperer le depart en DB
            d = entityManager.find(Depart.class, idDepart);

            if (d == null) {
                return null;
            } else {
                d.setVol(v);
                entityManager.flush();

                d = entityManager.find(Depart.class, d.getNumero());
            }
            entityTransaction.commit();
        }

        return d;
    }

    public List<Depart> findAll() {
        String queryStr = "SELECT d FROM Depart d";
        Query query = entityManager.createQuery(queryStr);
        return query.getResultList();
    }

    /**
     * Passer une date au format "dd/MM/yyyy"
     * Obtenir le depart associe a cette date
     * @param dateString String
     * @return
     */
    public List<Depart> findByDate(String dateString) {
        List<Depart> d = null;

        Date date = MainAeroport.createDate(dateString);

        String queryStr = "SELECT d FROM Depart d where d.date=:date";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("date", date);
        List<Depart> departs = query.getResultList();

        if (departs != null && !departs.isEmpty()) {
            d = departs;
        }

        return d;
    }

    public Depart findById(int id) {
        Depart d = null;
        String queryStr = "SELECT d FROM Depart d where d.numero=:num";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("num", id);
        List<Depart> departs = query.getResultList();

        if (departs != null && !departs.isEmpty()) {
            d = departs.get(0);
        }

        return d;
    }

    public Depart findDepartEmbarquement(List<Depart> listDepart){
        // affichage liste des departs
        for(Depart d : listDepart){
            LOGGER.info(d);
        }

        // id
        LOGGER.info("Saisir l'identifiant du départ:");
        int id;
        do {
            id = MainAeroport.sc.nextInt();
        } while (!listDepart.contains(findById(id)));

        return findById(id);
    }
}
