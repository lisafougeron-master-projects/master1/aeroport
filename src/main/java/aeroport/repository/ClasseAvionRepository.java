package aeroport.repository;

import aeroport.avion.ClasseAvion;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class ClasseAvionRepository {
    private EntityManager entityManager;

    public ClasseAvionRepository() {
    }

    public ClasseAvionRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public ClasseAvion save(ClasseAvion ca) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.persist(ca);
        ClasseAvion e = entityManager.find(ClasseAvion.class, ca.getNumero());
        entityTransaction.commit();

        return e;
    }

    public List<ClasseAvion> findAll() {
        String queryStr = "SELECT ca FROM ClasseAvion ca";
        Query query = entityManager.createQuery(queryStr);
        return query.getResultList();
    }

    public ClasseAvion findOne(int id) {
        ClasseAvion ca = null;
        String queryStr = "SELECT ca FROM ClasseAvion ca where ca.numero=:num";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("num", id);
        List<ClasseAvion> cas = query.getResultList();

        if (cas != null && !cas.isEmpty()) {
            ca = cas.get(0);
        }

        return ca;
    }
}
