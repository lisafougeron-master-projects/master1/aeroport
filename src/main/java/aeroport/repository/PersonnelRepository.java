package aeroport.repository;

import aeroport.MainAeroport;
import aeroport.personne.Personnel;
import aeroport.personne.Service;
import aeroport.vol.Depart;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class PersonnelRepository {
    private static final Logger LOGGER = Logger.getLogger(PersonnelRepository.class);
    private EntityManager entityManager;

    public PersonnelRepository() {
    }

    public PersonnelRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Personnel save(Personnel personnel) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.persist(personnel);
        Personnel pers = entityManager.find(Personnel.class, personnel.getNumero());
        entityTransaction.commit();

        return pers;
    }

    /**
     * Retourne la liste des Personnels
     *
     * @return  List    Liste des personnels existants
     */
    public List<Personnel> findAll() {
        String queryStr = "SELECT p FROM Personnel p";
        Query query = entityManager.createQuery(queryStr);
        return query.getResultList();
    }

    public boolean addPersonnel() {
        boolean hasInserted = false;

        LOGGER.info("---- Interface d'ajout d'un Personnel ----");
        Personnel personnel = setSaisiePersonnel();

        // Choix du SERVICE
        LOGGER.info("Choix service Personnel");
        Service service = choiceService();

        if (service != null) {
            Personnel savedPers = save(personnel);
            hasInserted = savedPers.equals(personnel);
        }

        return hasInserted;
    }

    public Service choiceService() {
        ServiceRepository serviceRepository = new ServiceRepository(entityManager);
        List<Service> serviceList = serviceRepository.findAll();

        Service serviceSelected = null;

        // On vérifie si la liste des services est remplie
        if (serviceList != null && !serviceList.isEmpty()) {
            // on récupère la valeur max du service pour connaitre la saisie max
            Service service1 = serviceList.get(serviceList.size() - 1);
            int maxId = service1.getNumero();

            LOGGER.info("Sélectionner le service auquel affecter le Personnel");

            for (Service service : serviceList) {
                LOGGER.info(service.getNumero() + " - " + service.getLibelle());
            }

            int serviceId = 0;

            do {
                serviceId = MainAeroport.sc.nextInt();

                for (int i = 0; i < serviceList.size(); i++) {
                    Service serv = serviceList.get(i);

                    if (serv.getNumero() == serviceId) {
                        serviceSelected = serv;
                        break;
                    }
                }
            } while (serviceId <= 0 || serviceId > maxId);

            assert serviceSelected != null;
            LOGGER.info("Vous avez sélectionné le service : "+ serviceSelected.getLibelle());

        // Si elle est vide, on le signale à l'utilisateur
        } else {
            LOGGER.info("Vous devez créer des services pour ajouter des Personnels. Abandon ajout Personnel");
        }

        return serviceSelected;

    }


    public Personnel setSaisiePersonnel() {
        // NOM
        LOGGER.info("Nom Personnel :");
        String nom;

        do {
            nom = MainAeroport.sc.nextLine();
        } while (nom == null || nom.isEmpty());


        // PRENOM
        LOGGER.info("Prénom Personnel :");
        String prenom;

        do {
            prenom = MainAeroport.sc.nextLine();
        } while (prenom == null || prenom.isEmpty());


        // ADRESSE
        LOGGER.info("Adresse Personnel :");
        String adresse;

        do {
            adresse = MainAeroport.sc.nextLine();
        } while (adresse == null || adresse.isEmpty());


        // NUM TEL
        LOGGER.info("Numéro Tel Personnel :");
        long numTel = 0;

        do {
            numTel = MainAeroport.sc.nextLong();
        } while (numTel <= 0);


        return new Personnel(nom, prenom, adresse, numTel);
    }

    public void listPersonnels() {
        List<Personnel> personnelList = findAll();

        for (Personnel personnel : personnelList) {
            LOGGER.info(personnel.getNumero() + " - " + personnel.getNom() + ", "+ personnel.getPrenom());
        }
    }

    public boolean associerPersonnel(Personnel personnel, Depart depart) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        depart.getPersonnels().add(personnel);
        personnel.getDeparts().add(depart);
        entityManager.persist(personnel);
        entityManager.persist(depart);
        entityTransaction.commit();
        return true;
    }
}
