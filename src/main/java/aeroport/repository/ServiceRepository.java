package aeroport.repository;

import aeroport.MainAeroport;
import aeroport.personne.Service;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class ServiceRepository {
    private static final Logger LOGGER = Logger.getLogger(ServiceRepository.class);
    private EntityManager entityManager;
    private EntityTransaction entityTransaction = null;

    public ServiceRepository() {
    }

    public ServiceRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private EntityTransaction getInstanceTransaction() {
        if (entityTransaction == null) {
           entityTransaction = entityManager.getTransaction();
        }

        return entityTransaction;
    }

    /**
     * Ajout d'un service
     *
     * @param service   service à ajouter
     * @return          un service
     */
    public Service save(Service service) {
        getInstanceTransaction().begin();

        entityManager.persist(service);
        Service serv = entityManager.find(Service.class, service.getNumero());
        getInstanceTransaction().commit();

        return serv;
    }

    /**
     * Recherche d'un service par son nom
     *
     * @param name  nom du service
     * @return      Service
     */
    public Service findByName(String name) {
        Service service = null;

        // Exécution de la requête et récupération résultat
        String queryStr = "SELECT s.numero, s.libelle FROM Service s WHERE s.libelle LIKE :nom";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("nom", "%" + name + "%");

        List<Service> res = query.getResultList();

        if (res != null && !res.isEmpty()) {
            service = res.get(0);
        }

        return service;
    }

    /**
     * Retourne la liste des Services
     *
     * @return  List    Liste des services existants
     */
    public List<Service> findAll() {
        String queryStr = "SELECT s FROM Service s";
        Query query = entityManager.createQuery(queryStr);
        return query.getResultList();
    }


    /**
     * Méthode d'ajout d'un Service
     *
     * @return boolean true si le service a été inséré
     */
    public boolean addService() {
        LOGGER.info("---- Interface de Gestion des Services ----");
        LOGGER.info("Ajout d'un service");
        LOGGER.info("Nom du service : ");


        String serviceName;
        boolean hasInserted = false;

        do {
            serviceName = MainAeroport.sc.nextLine();
        } while (serviceName == null || serviceName.isEmpty());


        // Avant d'ajouter le service, on vérifie que le service n'est pas déjà présent dans la liste
        Service serviceToSearch = findByName(serviceName);

        if (serviceToSearch == null) {
            Service service = new Service(serviceName);
            Service serv = save(service);

            hasInserted = serv.equals(service);
        } else {
            LOGGER.info("Service déjà inséré en base !");
        }

        return hasInserted;
    }

    public void listServices() {
        List<Service> serviceList = findAll();

        for (Service service : serviceList) {
            LOGGER.info(service.getNumero() + " - " + service.getLibelle());
        }
    }

}
