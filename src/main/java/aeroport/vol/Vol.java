package aeroport.vol;


import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(schema = "AEROPORT")
public class Vol {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int numero;
    private int frequence;

    /**
     * Constructeur par défaut
     */
    public Vol() {
    }

    /**
     * Constructeur
     *
     * @param frequence int
     */
    public Vol(int frequence) {
        this.frequence = frequence;
    }

    /**
     * Retourne le numéro du vol
     *
     * @return numéro de vol
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Modifie le numéro de vol
     *
     * @param numero int - Nouveau numéro de vol
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Retourne la fréquence du vol
     *
     * @return la fréquence du vol
     */
    public int getFrequence() {
        return frequence;
    }

    /**
     * Modifie la fréquence du vol
     *
     * @param frequence int - nouvelle fréquence de vol
     */
    public void setFrequence(int frequence) {
        this.frequence = frequence;
    }

    /**
     * toString
     * @return String
     */
    public String toString() {
        return "Vol numéro : " + this.getNumero() + " | Fréquence : " + this.getFrequence();
    }
}
