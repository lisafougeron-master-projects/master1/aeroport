package aeroport.vol;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(schema = "AEROPORT")
public class VolTroncon {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int numero;
    private Date heureArrivee;
    private Date heureDepart;

    @OneToOne
    private Vol vol;

    @OneToOne
    private Troncon troncon;

    /**
     * Constructeur par défaut
     */
    public VolTroncon() {
    }

    /**
     * Constructeur
     *
     * @param heureArrivee  Date
     * @param heureDepart   Date
     * @param vol           Vol
     * @param troncon       Troncon
     */
    public VolTroncon(Date heureArrivee, Date heureDepart, Vol vol, Troncon troncon) {
        this.heureArrivee = heureArrivee;
        this.heureDepart = heureDepart;
        this.vol = vol;
        this.troncon = troncon;
    }

    /**
     * Retourne le numéro de VolTroncon
     *
     * @return le numéro de VolTroncon
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Modifie le numéro de VolTroncon
     *
     * @param numero int - Nouveau numéro de VolTroncon
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Retourne l'heure d'arrivée du VolTroncon
     *
     * @return l'heure d'arrivée du VolTroncon
     */
    public Date getHeureArrivee() {
        return heureArrivee;
    }

    /**
     * Modifie l'heure d'arrivée du VolTroncon
     *
     * @param heureArrivee  Date - Nouvelle heure d'arrivée du VolTroncon
     */
    public void setHeureArrivee(Date heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    /**
     * Retourne l'heure de départ du VolTroncon
     *
     * @return  l'heure de départ du VolTroncon
     */
    public Date getHeureDepart() {
        return heureDepart;
    }

    /**
     * Modifie l'heure de départ du VolTroncon
     *
     * @param heureDepart   Date - Nouvelle heure de départ du VolTroncon
     */
    public void setHeureDepart(Date heureDepart) {
        this.heureDepart = heureDepart;
    }

    /**
     * Retourne le vol du VolTroncon
     *
     * @return  le vol du VolTroncon
     */
    public Vol getVol() {
        return vol;
    }

    /**
     * Modifie le vol du VolTroncon
     *
     * @param vol   Vol - Nouveau vol du VolTroncon
     */
    public void setVol(Vol vol) {
        this.vol = vol;
    }

    /**
     * Retourne le troncon du VolTroncon
     *
     * @return  le troncon du VolTroncon
     */
    public Troncon getTroncon() {
        return troncon;
    }

    /**
     * Modifie le troncon du VolTroncon
     *
     * @param troncon   Troncon - Nouveau troncon du VolTroncon
     */
    public void setTroncon(Troncon troncon) {
        this.troncon = troncon;
    }
}
