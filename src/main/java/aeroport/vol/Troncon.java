package aeroport.vol;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(schema = "AEROPORT")
public class Troncon {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int numero;
    private String villeDepart;
    private String villeArrivee;
    private long distance;

    /**
     * Constructeur par défaut
     */
    public Troncon() {
    }

    /**
     * Constructeur
     *
     * @param villeDepart  String
     * @param villeArrivee String
     * @param distance     long
     */
    public Troncon(String villeDepart, String villeArrivee, long distance) {
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
        this.distance = distance;
    }

    /**
     * Retourne le numero du troncon
     *
     * @return le numero du troncon
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Modifie le numero du troncon
     *
     * @param numero int - Nouveau numero du troncon
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }


    /**
     * Retourne la ville de départ du troncon
     *
     * @return la ville de départ du troncon
     */
    public String getVilleDepart() {
        return villeDepart;
    }

    /**
     * Modifie la ville de départ du troncon
     *
     * @param villeDepart String - Nouvelle ville de départ
     */
    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }

    /**
     * Retourne la ville d'arrivée du troncon
     *
     * @return la ville d'arrivée du troncon
     */
    public String getVilleArrivee() {
        return villeArrivee;
    }

    /**
     * Modifie la ville de départ du troncon
     *
     * @param villeArrivee String - Nouvelle ville d'arrivée
     */
    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    /**
     * Retourne la distance du troncon
     *
     * @return la distance du troncon
     */
    public long getDistance() {
        return distance;
    }

    /**
     * Modifie la distance du troncon
     *
     * @param distance long - distance du troncon
     */
    public void setDistance(long distance) {
        this.distance = distance;
    }

    public String toString() {
        return "Troncon numero : " + this.getNumero() + " | Ville depart : " + this.getVilleDepart() + " | Ville arrivee : " + this.getVilleArrivee() + " | distance : " + this.getDistance();
    }
}
