package aeroport.vol;

import aeroport.avion.DepartAvion;
import aeroport.personne.Passager;
import aeroport.personne.Personnel;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(schema = "AEROPORT")
public class Depart {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int numero;
    private Date date;

    @ManyToMany(cascade = {CascadeType.PERSIST})
    @JoinTable(name = "ASS_DEP_PERS", schema = "AEROPORT",
            joinColumns = @JoinColumn(name = "DEPART_ID"),
            inverseJoinColumns = @JoinColumn(name = "PERSONNEL_ID"))
    private Collection<Personnel> personnels;


    @ManyToMany(cascade = {CascadeType.PERSIST})
    @JoinTable(name = "ASS_DEP_PASS", schema = "AEROPORT",
            joinColumns = @JoinColumn(name = "DEPART_ID"),
            inverseJoinColumns = @JoinColumn(name = "PASSAGER_ID"))
    private Collection<Passager> passagers;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private DepartAvion departAvion;

    @OneToOne
    private Vol vol;

    /**
     * Constructeur
     *
     * @param date
     * @param personnels
     * @param passagers
     */
    public Depart(Date date, Collection<Personnel> personnels, Collection<Passager> passagers) {
        this.date = date;
        this.personnels = personnels;
        this.passagers = passagers;
    }

    /**
     * Constructeur
     *
     * @param date
     */
    public Depart(Date date) {
        this.date = date;
        this.personnels = new ArrayList<>();
        this.passagers = new ArrayList<>();
    }

    /**
     * Constructeur
     *
     * @param date        Date
     * @param personnels  Collection<Personnel>
     * @param passagers   Collection<Passager>
     * @param departAvion DepartAvion
     */
    public Depart(Date date, Collection<Personnel> personnels, Collection<Passager> passagers, DepartAvion departAvion) {
        this.date = date;
        this.personnels = personnels;
        this.passagers = passagers;
        this.departAvion = departAvion;
    }

    /**
     * Constructeur par defaut
     */
    public Depart() {
    }

    /**
     * Obtenir numero de depart
     *
     * @return numero de depart
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Obtenir date de depart
     *
     * @return date de depart
     */
    public Date getDate() {
        return date;
    }

    /**
     * Definir date de depart
     *
     * @param date Date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Obtenir Collection de personnels du depart
     *
     * @return Collection de personnels du depart
     */
    public Collection<Personnel> getPersonnels() {
        return personnels;
    }

    /**
     * Definir Collection de personnels du depart
     *
     * @param personnels Collection<Personnel>
     */
    public void setPersonnels(Collection<Personnel> personnels) {
        this.personnels = personnels;
    }

    /**
     * Obtenir Collection de passager du depart
     *
     * @return Collection de passager du depart
     */
    public Collection<Passager> getPassagers() {
        return passagers;
    }

    /**
     * Definir Collection de passager du depart
     *
     * @param passagers Collection<Passager>
     */
    public void setPassagers(Collection<Passager> passagers) {
        this.passagers = passagers;
    }

    /**
     * Obtenir DepartAvion du depart
     *
     * @return DepartAvion
     */
    public DepartAvion getDepartAvion() {
        return departAvion;
    }

    /**
     * Definir DepartAvion du depart
     *
     * @param departAvion DepartAvion
     */
    public void setDepartAvion(DepartAvion departAvion) {
        this.departAvion = departAvion;
    }

    public String toString() {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        return "Depart numero : " + this.getNumero() + " | Date : " + dateformat.format(this.getDate()) + " | Nb Personnel : " + this.getPersonnels().size() + " | Nb Passager : " + this.getPassagers().size();
    }

    public Vol getVol() {
        return vol;
    }

    public void setVol(Vol vol) {
        this.vol = vol;
    }

    /**
     * Ajout d'un personnel à la liste des personnels d'un Départ
     *
     * @param personnel Personnel
     */
    public void addPersonnel(Personnel personnel) {
        this.getPersonnels().add(personnel);
    }
}
