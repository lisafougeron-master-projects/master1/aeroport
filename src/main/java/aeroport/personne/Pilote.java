package aeroport.personne;

public class Pilote extends Navigant {

    private double nbHeureVol;

    public Pilote(String nom, String prenom, String adresse, long noTelephone, Service service, Grade grade) {
        super(nom, prenom, adresse, noTelephone, service, grade);
    }

    public Pilote(String nom, String prenom, String adresse, long noTelephone, Service service, Grade grade, double nbHeureVol) {
        super(nom, prenom, adresse, noTelephone, service, grade);
        this.nbHeureVol = nbHeureVol;
    }

    public double getNbHeureVol() {
        return nbHeureVol;
    }

    public void setNbHeureVol(double nbHeureVol) {
        this.nbHeureVol = nbHeureVol;
    }
}
