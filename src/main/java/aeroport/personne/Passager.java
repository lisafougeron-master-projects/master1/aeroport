package aeroport.personne;

import aeroport.vol.Depart;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(schema = "AEROPORT")
public class Passager extends Personne {

    private long numPasseport;

    @ManyToMany(mappedBy = "passagers")
    private Collection<Depart> departs;

    /**
     * Constructeur par Defaut
     */
    public Passager() {
    }


    /**
     * Constructeur permettant d'instancier un Passager
     *
     * @param nom          le nom du Passager, type : String
     * @param prenom       le prenom du Passager, type : String
     * @param adresse      l adresse du Passager, type : String
     * @param noTelephone  le numero de telephone du Passager, type : long
     * @param numPasseport le numero de passeport du Passager, type : long
     */
    public Passager(String nom, String prenom, String adresse, long noTelephone, long numPasseport) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.noTelephone = noTelephone;
        this.numPasseport = numPasseport;
        this.departs=new ArrayList<>();
    }

    // Getters / Setters

    public long getNumPasseport() {
        return numPasseport;
    }

    public void setNumPasseport(long numPasseport) {
        this.numPasseport = numPasseport;
    }

    public Collection<Depart> getDeparts() {
        return departs;
    }

    public void setDeparts(Collection<Depart> departs) {
        this.departs = departs;
    }

    @Override
    public String toString() {
        return "Passager{" +
                "numPasseport=" + numPasseport +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }

    public String printPassager() {
        return "Le passager " + nom +" "+prenom+" , numéro de passeport : "+numPasseport;
    }
}

