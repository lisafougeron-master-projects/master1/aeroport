package aeroport.personne;


import aeroport.vol.Depart;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(schema = "AEROPORT")
public class Personnel extends Personne {

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Service service;

    @ManyToMany(mappedBy = "personnels")
    private Collection<Depart> departs;

    /**
     * Constructeur par défaut
     */
    public Personnel() {

    }


    public Personnel(String nom, String prenom, String adresse, long noTelephone) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.noTelephone = noTelephone;
    }

    /**
     * Constructeur permettant d'instancier un Personnel
     * associer à un Service
     *
     * @param nom         Nom du Personnel
     * @param prenom      Prénom du Personnel
     * @param adresse     Adresse habitation Personnel
     * @param noTelephone Numero de téléphone du Personnel
     * @param service     Service auquel le Personnel est affecté
     */
    public Personnel(String nom, String prenom, String adresse, long noTelephone, Service service) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.noTelephone = noTelephone;
        this.service = service;
        this.departs = new ArrayList<>();
    }



    /**
     * Retourne le Service auquel est affecté le Personnel
     *
     * @return Service
     */
    public Service getService() {
        return service;
    }

    /**
     * Modifie le Service auquel est affecté le Personnel
     *
     * @param service Nouveau Service dans lequel est affecté le Personnel
     */
    public void setService(Service service) {
        this.service = service;
    }

    public Collection<Depart> getDeparts() {
        return departs;
    }

    public void setDeparts(Collection<Depart> departs) {
        this.departs = departs;
    }
}
