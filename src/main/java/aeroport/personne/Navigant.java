package aeroport.personne;

public class Navigant extends Personnel {

    private Grade grade;

    public Navigant(String nom, String prenom, String adresse, long noTelephone, Service service, Grade grade) {
        super(nom, prenom, adresse, noTelephone, service);
        this.grade = grade;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }
}
