package aeroport.personne;

import aeroport.MainAeroport;
import aeroport.avion.Avion;
import aeroport.avion.ClasseAvion;
import aeroport.exceptions.*;
import aeroport.repository.AvionRepository;
import aeroport.repository.VolRepository;
import aeroport.vol.Depart;
import aeroport.vol.Troncon;
import aeroport.vol.Vol;

import java.util.Collection;
import java.util.Date;

public class NonNavigant extends Personnel {

    /**
     * Constructeur par defaut
     */
    public NonNavigant() {
    }

    /**
     * Constructeur
     *
     * @param nom         String
     * @param prenom      String
     * @param adresse     String
     * @param noTelephone long
     * @param service     Service
     */
    public NonNavigant(String nom, String prenom, String adresse, long noTelephone, Service service) {
        super(nom, prenom, adresse, noTelephone, service);
    }

    /**
     * Creer un vol
     *
     * @param frequence int
     */
    public Vol createVol(int frequence) throws ExceptionServiceCreateVol {
        if (isGestionnaire()) {
            //Creation du vol et insertion
            return new Vol(frequence);
        } else {
            throw new ExceptionServiceCreateVol();
        }
    }

    public Vol modifVol(int frequence, Vol v) throws ExceptionServiceModifVol {
        if (isGestionnaire()) {
            VolRepository vr = new VolRepository(MainAeroport.entityManager);
            return vr.modifVol(frequence, v);
        } else {
            throw new ExceptionServiceModifVol();
        }
    }

    public Vol modifVol(int id) throws ExceptionServiceModifVol {
        if (isGestionnaire()) {
            VolRepository vr = new VolRepository(MainAeroport.entityManager);
            return vr.modifVol(id);
        } else {
            throw new ExceptionServiceModifVol();
        }
    }


    public Troncon createTroncon(String villeDepart, String villeArrivee, long distance) throws ExceptionServiceCreateTroncon {
        if (isGestionnaire()) {
            //Creation du vol et insertion
            return new Troncon(villeDepart, villeArrivee, distance);
        } else {
            throw new ExceptionServiceCreateTroncon();
        }
    }

    public Depart createDepart(Date date, Collection<Personnel> personnels, Collection<Passager> passagers) throws ExceptionServiceCreateDepart {
        if (isGestionnaire()) {
            //Creation du vol et insertion
            return new Depart(date, personnels, passagers);
        } else {
            throw new ExceptionServiceCreateDepart();
        }
    }

    /**
     * Creer une ClasseAvion
     *
     * @param libelle
     */
    public ClasseAvion createClasseAvion(String libelle) throws ExceptionServiceCreateClasseAvion {
        if (isTechnicien()) {
            //Creation de la classeAvion
            return new ClasseAvion(libelle);
        } else {
            throw new ExceptionServiceCreateClasseAvion();
        }
    }

    /**
     * Creer un Avion
     *
     * @param
     */
    public Avion createAvion(int capacite) throws ExceptionServiceCreateAvion {
        if (isTechnicien()) {
            //Creation de l'avion
            return new Avion(capacite);
        } else {
            throw new ExceptionServiceCreateAvion();
        }
    }

    public Avion modifAvion(int capacite, Avion a) throws ExceptionServiceModifAvion {
        if (isTechnicien()) {
            AvionRepository ar = new AvionRepository(MainAeroport.entityManager);
            return ar.modifAvion(capacite, a);
        } else {
            throw new ExceptionServiceModifAvion();
        }
    }

    private boolean isGestionnaire() {
        return this.getService().getLibelle().equals("Gestionnaire");
    }

    private boolean isTechnicien() {
        return this.getService().getLibelle().equals("Technique");
    }
}
