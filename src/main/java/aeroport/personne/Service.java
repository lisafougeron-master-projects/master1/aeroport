package aeroport.personne;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(schema = "AEROPORT")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int numero;
    private String libelle;

    @OneToMany(mappedBy = "service")
    private Collection<Personnel> personnels = new ArrayList<>();


    public Service() {
    }

    public Service(String libelle) {
        this.libelle = libelle;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
