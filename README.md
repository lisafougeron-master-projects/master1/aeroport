# Université de La Rochelle (2018-2019)
## Master 1 - UE Ingénierie logicielle
### Projet Agile : Mise en place d'une gestion d'un aéroport
### Technologies :
- Java
- Maven
- JPA
- SonarQube
### Auteurs
- Allusse Martin
- Baret Alexandre
- Fougeron Lisa
- Lambert Thibaud 
